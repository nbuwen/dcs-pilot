---   Configure  This   ---
---                     ---

local HOST = '127.0.0.1'
local SERVER_PORT = 3232
local REMOTE_PORT = 3223
local LOG_PATH = 'C:\\Users\\%s\\Desktop\\dcs-pilot.log'

local UPDATE_INTERVAL_IN_SECONDS = 1.0

---                     ---
--- No Edits Below Here ---

local AXIS_RANGE = 2000
local PYTHON_CMD_LENGTH = 100
local update_interval = UPDATE_INTERVAL_IN_SECONDS

--- Logging

function logf(pattern, ...)
    logger:write(string.format(pattern .. '\n', ...))
    logger:flush()
end

function log(list)
    for _, item in ipairs(list) do
        if item == nil then item = '<nil>' end
        logger:write(item)
        logger:write(' ')
    end
    logger:write('\n')
    logger:flush()
end

--- UDP Communication

function setup()
    local username = os.getenv('USERNAME')
    local path = '/mnt/f/dev/uni/dcs-pilot/dcs-pilot.log'
    if username ~= nil then
        path = string.format(LOG_PATH, username)
    end
    logger = io.open(path, 'w')

    package.path = package.path .. ";.\\LuaSocket\\?.lua"
    package.cpath = package.cpath .. ";.\\LuaSocket\\?.dll"
    socket = require('socket')
    server = socket.udp()
    client = socket.udp()

    server:setsockname(HOST, SERVER_PORT)
    server:settimeout(0)
end

function cleanup()
    server:close()
    client:close()
end

function send(data)
    client:sendto(data, HOST, REMOTE_PORT)
end

--- Protocol

function receive_command()
    data, err = server:receive(PYTHON_CMD_LENGTH)

    if data == nil then
        return false
    end

    local command = tonumber(string.sub(data, 1, 4))
    if command == nil then
        log{"Error", "ProtocolError", data}
        return false
    end

    if command < 0 then
        update_interval = -command
        return true
    elseif command < AXIS_RANGE then
        LoSetCommand(command)
        return true
    end

    local value = tonumber(string.sub(data, 6))
    if value == nil then
        log{"Error", "ProtocolError", data}
        return false
    end

    LoSetCommand(command, value)

    return true
end

function send_state_update(t)
    local pitch, roll, yaw = LoGetADIPitchBankYaw()
    local nav = LoGetNavigationInfo()

    if nav == nil or nav.Requirements == nil then
        nav = {
            pitch = 0, roll = 0,
            speed = 0, altitude = 0,
            vertical_speed = 0
        }
    else
        nav = nav.Requirements
    end

    local data = {
        t,
        yaw or 0, pitch or 0, roll or 0,
        LoGetIndicatedAirSpeed() or 0,
        LoGetAltitudeAboveSeaLevel() or 0,
        LoGetVerticalVelocity() or 0,

        nav.pitch or 0, nav.roll or 0,
        nav.speed or 0, nav.altitude or 0,
        nav.vertical_speed or 0
    }

    send(table.concat(data, ','))
end

--- DCS Events

function LuaExportStart()
    setup()
end

function LuaExportStop()
    cleanup()
end

function LuaExportActivityNextEvent(t)
    send_state_update(t)

    return t + update_interval
end

function LuaExportBeforeNextFrame()
    while receive_command() do end
end


--- Tests

function Test()
    setup()
    while true do
        local start = socket.gettime()
        send('1,2,3,4,5,6,7,8,9,10,11,12,13')
        local stop = socket.gettime()

        print(stop - start)
        socket.sleep(1)
    end
    cleanup()
end

if os.getenv('USERNAME') == nil then
    Test()
end