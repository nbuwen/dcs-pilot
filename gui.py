from PyQt5.QtWidgets import QApplication

from dcs.mainwindow import MainWindow
from dcs.qtserver import QtServer


def main():
    app = QApplication([])

    win = MainWindow()
    win.show()

    server = QtServer('localhost', 3232)

    server.data.connect(win.data)
    server.info.connect(win.info)
    win.command.connect(server.command)

    server.start()

    return app.exec_()


if __name__ == '__main__':
    main()
