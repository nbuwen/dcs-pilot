from socket import socket, AF_INET, SOCK_DGRAM, timeout
from collections import namedtuple
from threading import Thread
from queue import Queue, Empty

HOST = '127.0.0.1'
SERVER_PORT = 3223
REMOTE_PORT = 3232
DCS_STATE_BLOCK_LENGTH = 1000
TIMEOUT = 2


class Command:
    GearUp = 430
    GearDown = 431

    Throttle = 2004
    Yaw = 2003
    Pitch = 2001
    Roll = 2002

    Left = 1
    Right = -1

    Down = 1
    Up = -1

    Full = 1
    Idle = -1


Plane = namedtuple('Plane', 'yaw pitch roll ias altitude vertical_speed')
Navigation = namedtuple('Navigation', 'pitch roll ias altitude vertical_speed')
State = namedtuple('State', 'time plane navigation')


def state_from_list(data):
    plane = Plane(*data[1:7])
    nav = Navigation(*data[7:12])
    return State(data[0], plane, nav)


class ProtocolError(Exception):
    pass


class Receiver(Thread):
    def __init__(self, host, port, queue):
        super().__init__(name='python-dcs-receiver', daemon=True)
        self.address = host, port
        self.queue = queue

        self.server = socket(AF_INET, SOCK_DGRAM)
        self.server.settimeout(TIMEOUT)

    def run(self):
        self.server.bind(self.address)

        while True:
            try:
                self.receive()
            except timeout:
                pass

    def receive(self):
        data = self.server.recv(DCS_STATE_BLOCK_LENGTH)
        self.parse(data.decode())

    def parse(self, data):
        self.queue.put(state_from_list([float(datum) for datum in data.split(',')]))


class Sender(Thread):
    def __init__(self, host, port, queue):
        super().__init__(name='python-dcs-sender', daemon=True)
        self.address = host, port
        self.queue: Queue = queue

        self.client = socket(AF_INET, SOCK_DGRAM)

    def run(self):
        while True:
            try:
                data = self.queue.get(timeout=TIMEOUT)
            except Empty:
                pass
            else:
                self.send(data)

    def send(self, data):
        code, value = data
        message = f'{code:04}={value}'.encode()
        self.client.sendto(message, self.address)


class DCS:
    def __init__(self, host='127.0.0.1', server_port=3223, remote_port=3232):
        self.command_queue = Queue()
        self.state_queue = Queue()

        self.receiver = Receiver(host, server_port, self.state_queue)
        self.sender = Sender(host, remote_port, self.command_queue)

        self.show_timeout = True
        self.running = False

    def start(self):
        self.receiver.start()
        self.sender.start()

    def send_command(self, command, value=0.0):
        self.command_queue.put((command, -value))

    def send_update_rate(self, rate):
        self.send_command(-rate)

    def states(self):
        if not self.running:
            self.start()

        try:
            while True:
                try:
                    yield self.state_queue.get(timeout=TIMEOUT)
                except Empty:
                    if self.show_timeout:
                        print("Timeout")
        except KeyboardInterrupt:
            print("Cancelled")


if __name__ == '__main__':
    from datetime import datetime

    dcs = DCS()
    dcs.send_update_rate(0.1)
    for state in dcs.states():
        print(datetime.now().strftime('%H:%M:%S.%f'), state.time)
        print('   ', state.plane)
        print('   ', state.navigation)
        dcs.send_command(Command.Throttle, Command.Full)
