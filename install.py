from os import mkdir, rename, system
from os.path import expanduser, exists, abspath
from sys import exit as sys_exit
from datetime import datetime


folders = 'DCS.openbeta', 'DCS'
games = expanduser('~\\Saved Games')
candidates = [
    f'{games}\\{folder}' for folder in folders
]

try:
    dcs = next(filter(exists, candidates))
except StopIteration:
    print("No DCS World installation found")
    sys_exit()

scripts = f'{dcs}\\Scripts'
    
try:
    mkdir(scripts)
except FileExistsError as e:
    pass

export = f'{scripts}\\Export.lua'
    
if exists(export):
    now = datetime.now().strftime('%d.%m.%Y.%H.%M.%S')
    alt = f'Export.{now}.lua'
    print(f"Export.lua already exists. Renaming to {alt}")
    rename(export, f'{scripts}\\{alt}')

print("Linking Export.lua")
real = abspath('exporter\\Export.lua')
system(f'mklink "{export}" "{real}"')
